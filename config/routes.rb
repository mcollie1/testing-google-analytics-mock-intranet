Rails.application.routes.draw do
 
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  get '/admin' => "rails_admin#maindashboard", as: 'dashboard'

  devise_for :users, controllers: {registrations: 'registrations'}
  
  # resources :posts
  # resources :topics
  # nesting posts to topics {restful conventions}
  resources :topics do
    resources :posts, except: [:index]
  end

  get 'about' => 'welcome#about'

  root to: 'welcome#index'

  
end
