class TopicsController < ApplicationController
  def index
  	#@topics = Topic.all
    @topics = Topic.limit(10).paginate(page: params[:page]) #, per_page: 10) see model
  	authorize @topics
  end

  def new
  	@topic = Topic.new
  	authorize @topic
  end

  def create
  	@topic = Topic.new(topic_params)
  	authorize @topic
  	if @topic.save
  		redirect_to @topic, notice: "Topic was saved successfully."
  	else
  		flash[:error] = "Error creating topic. Please try again."
       	render :new
  	end
  end

  def show
  	@topic = Topic.find(params[:id])
    #@posts = @topic.posts
    @posts = @topic.posts.limit(10).paginate(page: params[:page]) #, per_page: 10) see model
  	authorize @topic
  end

  def edit
  	@topic = Topic.find(params[:id])
  	authorize @topic
  end

  def update
  	@topic = Topic.find(params[:id])
  	authorize @topic
  	if @topic.update_attributes(topic_params)
  		redirect_to @topic
  	else
  		flash[:error] = "Error saving topic. Please try again."
  		render :edit
  	end
  end

  private

  def topic_params
    params.require(:topic).permit(:name, :description, :public)
  end

end
