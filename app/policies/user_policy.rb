class UserPolicy < ApplicationPolicy

	def create?
		user.present? && user.admin?
	end

	def new?
		create?
	end

	def update?
		user.present? && (user.admin? || current_user)
	end

	def edit?
		update?
	end

	def destroy?
		user.present? && user.admin? 
	end
end