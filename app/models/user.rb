class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable, taken out {:recoverable, }
  devise :database_authenticatable, :registerable, :rememberable, :trackable #:validatable

  has_many :posts

  def admin?
  	role == 'admin'
  end

  def moderator?
  	role == 'moderator'
  end

end
