class Topic < ActiveRecord::Base
	has_many :posts

	self.per_page = 10
	# set per_page globally
	WillPaginate.per_page = 10

end
