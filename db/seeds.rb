require 'faker'

# Create Posts

5.times do
	user = User.new(
		login:    Faker::Internet.email,
		password: Faker::Lorem.characters(10)
	)
	user.save!
end

user = User.first

# Create topics

15.times do
	Topic.create!(
		name: "Announcment: #{Faker::Lorem.sentence}",
		description: Faker::Lorem.paragraph
	)
end
topics = Topic.all

# Create posts

6000.times do 
	Post.create!(
		user: user,
		topic: topics.sample,
		title: "Accouncement: #{Faker::Lorem.sentence}",
		body: Faker::Lorem.paragraph
	)
end
posts = Post.all

# Create comments

100.times do
	Comment.create!(
		     # user: users.sample,   # we have not yet associated Users with Comments
		post: posts.sample,
		body: Faker::Lorem.paragraph
	)
end

user = User.first
user.update_attributes!(
	login: 'marcus.collier7@yahoo.com',
	password: 'helloworld',
	role: 'admin'
)

user = User.new(
	login: 'member@example.com',
	password: 'helloworld'
	)
user.save!

puts "Seed finished"
puts "#{User.count} users created"
puts "#{Post.count} posts created" 
puts "#{Comment.count} comments created"